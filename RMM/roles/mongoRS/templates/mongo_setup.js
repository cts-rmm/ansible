///////////////////////////////////////////
///////////////////////////////////////////
// NOTE manually keep files in scyn between repos
// ansible: https://bitbucket.org/cts-rmm/ansible/src/master/RMM/roles/mongoRS/templates/mongo_setup.js
// rmm: https://bitbucket.org/cts-rmm/rmm/src/master/scripts/database/mongo_setup.js
///////////////////////////////////////////
///////////////////////////////////////////
//
// To run, copy this to the box, and run this command
//
//     mongo localhost:27017/rmmdb mongo_setup.js
//
// On errors, mongo should print out something like this:
//
//     2016-11-11T20:01:45.062+0000 E QUERY    [thread1] TypeError: db.potato.we is not a function :
//     @mongo_setup.js:41:1
//     failed to load: mongo_setup.js
//
// On success, mongo should print `Finished with script`.
//
// To connect inside of the script, add these lines:
//
//    conn = new Mongo("localhost:27017");
//    db = conn.getDB("rmmdb");
//
// Example of mongo scripts from old ThisTech stuff:
// https://bitbucket.org/thistech/devops/src/a695f6cf2eed6ef68ab645acecfce398c63c41af/puppet/modules/tt_mongo/?at=master
// https://bitbucket.org/thistech/devops/src/a695f6cf2eed6ef68ab645acecfce398c63c41af/puppet/modules/tt_mongo/templates/indexes.json?at=master&fileviewer=file-view-default
//
//
// ## TTL ##
// We expire the records after three days. Fox should reload the database before then.
//
// 86400 is one day.
// 259200 is three days.W
// 604800 is seven days.
//
// For a TTL guides:
//    http://hassansin.github.io/working-with-mongodb-ttl-index
//
// Mongo ttl: https://docs.mongodb.com/manual/core/index-ttl/
//
// To check tll info
//    db.serverStatus().metrics.ttl
//
//
// ## Compound ##
// We are not using compound indexs.
// https://docs.mongodb.com/manual/core/index-compound/#index-type-compound
//
//
// ## Sparse Indexes" ## Sparse indexes only contain entries for documents that
// have the indexed field, even if the index field contains a null value. The
// index skips over any document that is missing the indexed field. The index is
// �sparse� because it does not include all documents of a collection.
// https://docs.mongodb.com/manual/core/index-sparse/



db = db.getSiblingDB('rmmdb');

db.media.dropIndexes()
db.media.createIndex({"writetime":1},{expireAfterSeconds:259200})
db.media.createIndex({ "id": 1 }, {"sparse" : true, unique: true })
// Media point ID might be duplicated. Don't do unique
db.mediapoint.dropIndexes()
db.mediapoint.createIndex({"writetime":1},{ expireAfterSeconds:259200})
db.mediapoint.createIndex({ "id": 1})
db.mediapoint.createIndex({ "matchtime" : 1 })
db.mediapoint.createIndex({ "source" : 1 })
db.mediapoint.createIndex({"signalid" : 1 })
db.mediapoint.createIndex({ "starttime" : 1 })
db.mediapoint.createIndex({ "msgid": 1 })
db.mediapoint.createIndex({ "airingid": 1 })
db.masemediapoint.dropIndexes()
db.masemediapoint.createIndex({ "id": 1 }, {"sparse" : true, unique: true })
db.masemediapoint.createIndex({"writetime":1},{ expireAfterSeconds:259200})
db.masemedia.dropIndexes()
db.masemedia.createIndex({ "id": 1 }, {"sparse" : true, unique: true })
db.masemedia.createIndex({"writetime":1},{ expireAfterSeconds:259200})
db.turnermediapoint.dropIndexes()
db.turnermediapoint.createIndex({ "id": 1 }, {"sparse" : true, unique: true })
db.turnermediapoint.createIndex({"writetime":1},{ expireAfterSeconds:259200})
db.turnermedia.dropIndexes()
db.turnermedia.createIndex({ "id": 1 }, {"sparse" : true, unique: true })
db.turnermedia.createIndex({"writetime":1},{ expireAfterSeconds:259200})
db.oldmediapoints.dropIndexes()
db.oldmediapoints.createIndex({"writetime":1},{expireAfterSeconds:86400})
db.oldmediapoints.createIndex({"id":1}, {"sparse":true})
db.policy.dropIndexes()
db.policy.createIndex({"writetime":1},{expireAfterSeconds:259200})
db.policy.createIndex({ "id": 1 }, { "sparse" : true, unique: true})
db.viewingpolicy.dropIndexes()
db.viewingpolicy.createIndex({"writetime":1},{expireAfterSeconds:259200})
db.viewingpolicy.createIndex({ "id": 1 }, { "sparse" : true, unique: true})
db.audience.dropIndexes()
db.audience.createIndex({"writetime":1},{expireAfterSeconds:259200})
db.audience.createIndex({ "id": 1 }, {"sparse" : true, unique: true })
// ingest
db.ingesthistory.dropIndexes()
db.ingesthistory.createIndex({"endtime":1},{expireAfterSeconds:259200})
db.ingesthistory.createIndex({"name":1})
db.ingesthistory.createIndex({"starttime":1})
db.ingesthistory.createIndex({"endtime":1})
db.ingesthistory.createIndex({"success":1})
db.ingesthistory.createIndex({"fail":1})
db.ingesthistory.createIndex({"failedids":1})
db.ingesthistory.createIndex({"retry":1})
db.ingesthistory.createIndex({"total":1})
db.ingesthistory.createIndex({"notes":1})
db.ingestfails.dropIndexes()
db.ingestfails.createIndex({"timestamp":1},{expireAfterSeconds:259200})
db.ingestfails.createIndex({ "name": 1})
db.ingestfails.createIndex({"msgid":1})
db.ingestfails.createIndex({ "name": 1, "msgid":1},{"sparse" : true, unique: true })
db.ingestfails.createIndex({"timestamp":1})
db.ingestfails.createIndex({"success":1})
db.ingestfails.createIndex({"notes":1})
db.ingestlogs.dropIndexes()
db.ingestlogs.createIndex({"timestamp":1},{expireAfterSeconds:259200})
db.ingestlogs.createIndex({"name": 1})
db.ingestlogs.createIndex({"msgid":1})
db.ingestlogs.createIndex({"msgid":1, "name": 1}, {"sparse" : true, unique: true })
db.ingestlogs.createIndex({"timestamp":1})
db.ingestlogs.createIndex({"success":1})
db.ingestlogs.createIndex({"notes":1})
// distro
db.distrohistory.dropIndexes()
db.distrohistory.createIndex({"endtime":1},{expireAfterSeconds:259200})
db.distrohistory.createIndex({"name":1})
db.distrohistory.createIndex({"starttime":1})
db.distrohistory.createIndex({"endtime":1})
db.distrohistory.createIndex({"success":1})
db.distrohistory.createIndex({"fail":1})
db.distrohistory.createIndex({"failedids":1})
db.distrohistory.createIndex({"retry":1})
db.distrohistory.createIndex({"total":1})
db.distrohistory.createIndex({"notes":1})
db.distrofails.dropIndexes()
db.distrofails.createIndex({"timestamp":1},{expireAfterSeconds:259200})
db.distrofails.createIndex({"name": 1})
db.distrofails.createIndex({"msgid":1})
db.distrofails.createIndex({"name": 1, "msgid":1}, {"sparse" : true, unique: true })
db.distrofails.createIndex({"timestamp":1})
db.distrofails.createIndex({"success":1})
db.distrofails.createIndex({"notes":1})
db.distrologs.dropIndexes()
db.distrologs.createIndex({"timestamp":1},{expireAfterSeconds:259200})
db.distrologs.createIndex({ "name": 1})
db.distrologs.createIndex({"msgid":1})
db.distrologs.createIndex({ "name": 1, "msgid":1}, {"sparse" : true, unique: true })
db.distrologs.createIndex({"timestamp":1})
db.distrologs.createIndex({"success":1})
db.distrologs.createIndex({"notes":1})





///////////
// No TTL items
///////////
db.source.dropIndexes()
db.source.ensureIndex({ "id": 1, "source" : 1 }, { "sparse" : true, unique: true} )
db.subscribers.dropIndexes()
db.subscribers.ensureIndex({ "id": 1 }, { "sparse" : true, unique: true} )
db.user.dropIndexes()
db.user.ensureIndex({ "id": 1 }, { "sparse" : true, unique: true} )
db.networks.dropIndexes()
db.networks.ensureIndex({ "id": 1 }, { "sparse" : true, unique: true} )
db.regions.dropIndexes()
db.regions.ensureIndex({ "id": 1 }, { "sparse" : true, unique: true} )
// Area is really big.  Don't drop indexes.
db.area.ensureIndex({ "id": 1 }, { "sparse" : true, unique: true} )
db.distributionareas.dropIndexes()
db.distributionareas.ensureIndex({ "id": 1 }, {"sparse" : true, unique: true })
db.maseaudience.dropIndexes()
db.maseaudience.ensureIndex({ "id": 1 }, { "sparse" : true, unique: true} )
db.masepolicy.dropIndexes()
db.masepolicy.ensureIndex({ "id": 1 }, { "sparse" : true, unique: true} )
db.maseviewingpolicy.dropIndexes()
db.maseviewingpolicy.ensureIndex({ "id": 1 }, { "sparse" : true, unique: true} )



print("Errors? (null if no errors):")
print(db.getLastError())
print("Finished with script")
